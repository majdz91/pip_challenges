from caesarcipher import CaesarCipher


def encrypt(realText, step):
    return CaesarCipher(realText, True, False, offset=step).encoded


def decrypt(text, step):
    return CaesarCipher(text, False, True, offset=step).decoded


def main():
    test = "THIS IS A TEST"
    enc = encrypt(test, 5)
    dec = decrypt(enc, 5)

    print(enc, dec)


if __name__ == '__main__':
    main()
