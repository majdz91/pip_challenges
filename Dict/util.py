import os
import random
from pathlib import Path
from string import digits

FILE_MAX_LINE = 300


class JsonGenerator(object):
    def __init__(self):
        self.numbers = [x for x in digits.split()]
        self.filename = 'file.json'

    def create_file(self):
        if os.path.isfile(self.filename):
            os.unlink(self.filename)
        Path(self.filename).touch()
        with open(self.filename, 'w') as file:
            for i in range(FILE_MAX_LINE):
                file.write("{},\n".format(str(random.randint(0, 9))))

        file.close()
