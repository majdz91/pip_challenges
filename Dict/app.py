from util import JsonGenerator
from counter import IntCounter


def main():
    j = JsonGenerator()
    j.create_file()
    a = IntCounter(j.filename)
    print(a.get_count_of(1))
    print(a.groups)


if __name__ == '__main__':
    main()
