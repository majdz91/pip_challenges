from os import path
from collections import Counter


class IntCounter(object):
    def __init__(self, filename):
        self.filename = filename
        self.data = []
        self.groups = []
        self.sort_data()

    def sort_data(self):
        if not path.isfile(self.filename):
            raise FileNotFoundError
        with open(self.filename, 'r') as source:
            line = source.read(source.__sizeof__() * 8).replace(',', '').replace('\n', '')
            self.data.append(line)
        source.close()

    def get_count_of(self, n):
        count = Counter(self.data)
        return count[n]
