import time
import subprocess
import cube
import config
from checker import Parser


def main():
    FILE_NAME = 'riddle_file.png'
    pattern = cube.Cube(3)
    pattern.randomize(55)
    face = pattern.describe()
    print("Creating Random Figure .. ")
    face.savefig(FILE_NAME, dpi=865 / pattern.N)
    source_parser = Parser(FILE_NAME)
    while True:
        try:
            print(source_parser.get_riddle())
            answer_file = input('Select an Image .. ')
            an_par = Parser(answer_file)
            assert an_par.block_colors == source_parser.block_colors
            print("Well Done!, Challenge completed!")
            exit(0)
        except AssertionError as e:
            print("Not Valid Solution ")
            if config.EASY:
                hint = input("Do you need hint ? [y|n] > ")
                if hint == "y":
                    p = subprocess.Popen(["display", source_parser.img.filename])
                    time.sleep(3)
                    p.kill()

        except FileNotFoundError:
            print('File Not Found! {}'.format(answer_file))
        except KeyboardInterrupt:
            print("killing .. ")
            exit(1)


if __name__ == '__main__':
    main()
