import config
from freegames import floor, square, vector


class Pattern(object):
    def __init__(self, block_colors: dict):
        self.block_colors = block_colors
        self.tiles = [
            [-400, 0, 200, self.block_color_to_string(block_colors[0])],  # 1
            [-200, 0, 200, self.block_color_to_string(block_colors[1])],  # 2
            [0, 0, 200, self.block_color_to_string(block_colors[2])],  # 3
            [-400, -200, 200, self.block_color_to_string(block_colors[3])],  # 4
            [-200, -200, 200, self.block_color_to_string(block_colors[4])],  # 5
            [0, -200, 200, self.block_color_to_string(block_colors[5])],  # 6
            [-400, -400, 200, self.block_color_to_string(block_colors[6])],  # 7
            [-200, -400, 200, self.block_color_to_string(block_colors[7])],  # 8
            [0, -400, 200, self.block_color_to_string(block_colors[8])],  # 9
        ]
        self.vector_order = {}
        for i in range(len(self.tiles)):
            self.vector_order.setdefault(i + 1, vector(self.tiles[i][0], self.tiles[i][1]))

    def block_color_to_string(self, block_color):
        if block_color == 'R':
            return 'red'
        elif block_color == 'G':
            return 'green'
        elif block_color == 'B':
            return 'blue'

    def get_tiles(self):
        return self.tiles

    def get_pattern(self):
        return [self.vector_order.get(i) for i in config.SIMON_SAYS_PATTERN]

    def get_vector_tile_color(self, vector: vector):
        for i in self.tiles:
            if vector.x == i[1] and vector.y == i[0]:
                return i[3]
