import config
import numpy as np
import image_slicer
from PIL import Image
from collections import defaultdict
from os import path


class Parser(object):

    def __init__(self, im: str):
        self.by_color = defaultdict(int)
        self.blocks = {}
        self.block_colors = {}
        if not path.isfile(im):
            raise FileNotFoundError("Could not fine file : {}".format(im))
        img = Image.open(im)
        self.img = img
        self.array = np.array(im)
        self.pixels = self.img.load()
        self.thumbnail = self.img.thumbnail((100, 100))
        self.crop_cells()
        img.close()

    def getByColor(self, img: Image):
        rgb_im = img.convert('RGB')
        width, height = img.size
        center = (width / 2, height / 2)
        r, g, b = rgb_im.getpixel(center)
        if r >= config.RED_RANGE_MIN and r <= config.RED_RANGE_MAX:
            return "R"
        if g >= config.GREEN_RANGE_MIN and g <= config.GREEN_RANGE_MAX:
            return "G"
        if b >= config.BLUE_RANGE_MIN and b <= config.BLUE_RANGE_MAX:
            return "B"

    def get_slice_color(self, slice: Image):
        return self.getByColor(slice)

    def crop_cells(self):
        if not self.blocks:
            img = image_slicer.slice(self.img.filename, 9, 3, 3, False)
            for one_slice in img:
                self.blocks.setdefault(one_slice.number, one_slice.image)
                self.block_colors.setdefault(one_slice.number, self.get_slice_color(one_slice.image))

    def colors_to_array(self):
        res = []
        for x in zip(*[iter(self.block_colors.items())] * 3):
            for z, y in x:
                res.append(y)
        return res

    def get_riddle(self):
        return "[\n" \
               "   [{}, {}. {}],\n" \
               "   [{}, {}, {}],\n" \
               "   [{}, {}, {}]\n" \
               "]".format(*self.colors_to_array())
