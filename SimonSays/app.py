from time import sleep
from turtle import *
from freegames import floor, square, vector
from util import Pattern
import checker

pattern = []
guesses = []
tiles = {
    vector(0, 0): ('red', 'dark red'),
    vector(-400, 0): ('red', 'dark red'),
    vector(0, -200): ('blue', 'dark blue'),
    vector(-200, 0): ('green', 'dark green'),
    vector(-200, -200): ('yellow', 'khaki'),
    vector(-200, -400): ('yellow', 'khaki'),
    vector(-400, -200): ('yellow', 'khaki'),
    vector(-200, -400): ('yellow', 'green'),
    vector(0, -400): ('yellow', 'green'),
}
parser = checker.Parser('riddle_file.png')
riddle_pattern = Pattern(parser.colors_to_array())
pattern_sort = riddle_pattern.get_pattern()
pattern_sort.reverse()


def grid():
    for i in riddle_pattern.get_tiles():
        square(i[0], i[1], i[2], i[3])
    update()


def flash(tile):
    "Flash tile in grid."
    glow = riddle_pattern.get_vector_tile_color(tile)
    dark = 'dark {}'.format(glow)
    square(tile.x, tile.y, 200, glow)
    update()
    sleep(0.5)
    square(tile.x, tile.y, 200, dark)
    update()
    sleep(0.5)


def grow():
    "Grow pattern and flash tiles."
    tile = pattern_sort.pop()
    pattern.append(tile)

    for tile in pattern:
        flash(tile)

    print('Pattern length:', len(pattern))
    guesses.clear()


def tap(x, y):
    "Respond to screen tap."
    onscreenclick(None)
    x = floor(x, 200)
    y = floor(y, 200)
    tile = vector(x, y)
    index = len(guesses)

    if tile != pattern[index]:
        exit()

    guesses.append(tile)
    flash(tile)

    if len(guesses) == len(pattern):
        grow()

    onscreenclick(tap)


def start(x, y):
    "Start game."
    grow()
    onscreenclick(tap)


setup(840, 840, 370, 0)
hideturtle()
tracer(False)
grid()
onscreenclick(start)
done()
