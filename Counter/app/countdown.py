import os
import tkinter as tk


class Countdown(tk.Tk):

    def __init__(self, time_in_min, process_id):
        tk.Tk.__init__(self)
        self.wm_title("Caesar Cipher Timer")
        self.geometry("800x600")
        self.headline_label = tk.Label(self, text="Kill the Timer!", width=300, fg="blue", font=("Helvetica", 48), pady=100)
        self.headline_label.pack()
        self.pid_label = tk.Label(self, text="Process id (PID): {0}".format(process_id), width=300, font=("Helvetica", 48), pady=50)
        self.pid_label.pack()
        self.counter_label = tk.Label(self, text="", width=300, font=("Helvetica", 48), pady=50)
        self.counter_label.pack()
        self.remaining = 0
        self.countdown(time_in_min * 60)

    def countdown(self, remaining=None):
        if remaining is not None:
            self.remaining = remaining

        if self.remaining <= 0:
            self.counter_label.configure(text="Time is up!", width=300, font=("Helvetica", 48), pady=50, fg="red")
        else:
            minutes = int(self.remaining / 60)
            seconds = self.remaining % 60
            self.counter_label.configure(text="{0}:{1}".format(minutes, seconds))
            self.remaining -= 1
            self.after(1000, self.countdown)


app = Countdown(15, os.getpid())
app.mainloop()
