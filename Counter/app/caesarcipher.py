from flask import Flask, render_template
from random import randint
import os
import subprocess
import psutil

app = Flask(__name__)

TEXT = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc";


@app.route('/')
def load_page():
    cipher = cc_encode(TEXT)
    if cc_is_crackable(cipher):
        start_countdown()
        return render_template('caesar_cipher_page.html', text=cipher)
    return "404"


def cc_encode(text, shift=None):
    cipher_text = ""
    if not shift:
        shift = randint(1, 26)

    for c in text:
        if c.isalpha():
            c = c.lower()
            shifted_char = ord(c) + shift
            if shifted_char > ord('z'):
                shifted_char -= 26
            c = chr(shifted_char)

        cipher_text += c

    return cipher_text


def cc_decode(cipher, shift):
    original = ""
    for c in cipher:
        if c.isalpha():
            c = c.lower()
            reverse_shifted_char = ord(c) - shift
            if reverse_shifted_char < ord('a'):
                reverse_shifted_char += 26
            c = chr(reverse_shifted_char)
        original += c

    return original


def cc_is_crackable(cipher):
    for i in range(1, 27):
        if cc_decode(cipher, i) == TEXT.lower():
            return True
    return False


def start_countdown():
    counter_pid_file = os.path.dirname(__file__) + "/countdown_pid.txt"
    counter_py_script = os.path.dirname(__file__) + "/countdown.py"
    subproc_args = ["python3", counter_py_script]

    if os.path.exists(counter_pid_file):
        with open(counter_pid_file) as f:
            pid = f.readline()
            f.close()
        if pid.isnumeric() and psutil.pid_exists(int(pid)):
            p = psutil.Process(int(pid))
            if p.cmdline() == subproc_args:
                return True

    proc = subprocess.Popen(subproc_args)
    with open(counter_pid_file, "w") as f:
        f.write(str(proc.pid))
        f.close()


if __name__ == "__main__":
    app.run(debug=True)
