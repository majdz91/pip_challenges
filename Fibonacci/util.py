from collections import defaultdict


class Fibonacci(object):
    def __init__(self, n: int):
        self.n = n
        self.numbers = defaultdict(int)
        self.set_numbers()

    def set_numbers(self):
        for i in range(self.n):
            self.numbers.setdefault(i, self.get_fibonacci_number(i))

    def get_numbers(self):
        return self.numbers.items()

    def get_fibonacci_number(self, n):
        if n <= 1:
            return n
        else:
            return (self.get_fibonacci_number(n - 1) + self.get_fibonacci_number(n - 2))
